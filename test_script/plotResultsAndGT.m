clear all
clc
close all
%% Reading pc, pd and gt files
id = 292;
samplePc = pcread(strcat(int2str(id), '.ply') );
gtfile = strcat(int2str(id), 'GT.txt');
predfile = strcat(int2str(id), 'PD.txt');
fileIDgt = fopen(gtfile, 'rt');
fileIDpd = fopen(predfile, 'rt');
datagt = textscan(fileIDgt, '%s %d %d %f %d %d %d %d %f %f %f %f %f %f %f');
datapd = textscan(fileIDpd, '%s %d %d %f %d %d %d %d %f %f %f %f %f %f %f %f');
fclose(fileIDgt);
fclose(fileIDpd);
pcshow(samplePc);
disp(strcat('Objects in the scene with ID:  ', int2str(id)));
% for i=1:size(datagt{1},1) % For each onject in the scene
%     P = [datagt{1,12}(i) datagt{1,13}(i) datagt{1,14}(i)] ;   % you center point 
%     L = [datagt{1,11}(i) datagt{1,10}(i) datagt{1,9}(i)] ;  % your cube dimensions 
%     O = P-L/2 ;       % Get the origin of cube so that P is at center 
%     plotcube(L,O,.4,[1 0 0])
%     disp(datagt{1,1}(i))
% end
% for i=1:size(datapd{1},1) % For each onject in the scene
%     P = [datapd{1,12}(i) datapd{1,13}(i) datapd{1,14}(i)] ;   % you center point 
%     L = [datapd{1,11}(i) datapd{1,10}(i) datapd{1,9}(i)] ;  % your cube dimensions 
%     O = P-L/2 ;       % Get the origin of cube so that P is at center 
%     plotcube(L,O,.4,[0 1 0])
%     disp(datapd{1,1}(i))
% end