# SARD Synthetic Annotated Railway Dataset

Official Repository of the SARD (Synthetic Annotated Railway Dataset) for 3D object detection and classification on synthetic railway point clouds.

[Laboratory website](https://muse.uniroma3.it/)

## Download Link 

[Link here on mega.nz](https://mega.nz/file/dldTlIRA#HX-hnHYg5B4HTXeYPz6jh_vp0uP7FTUAlXv2SptXPis)

## If you use this dataset, please cite the paper

```
@INPROCEEDINGS{Neri_2022_EUVIP,
  author={Neri, M. and Battisti, F.},
  booktitle={2022 10th European Workshop on Visual Information Processing (EUVIP)}, 
  title={{3D Object Detection on Synthetic Point Clouds for Railway Applications}}, 
  year={2022},
  volume={},
  number={},
  pages={1-6},
  doi={10.1109/EUVIP53989.2022.9922901}}
```
