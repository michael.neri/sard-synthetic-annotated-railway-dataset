clear all
clc
close all
%% Setting up paths
% To select which simulation to run, edit Simulation block in simulink script
path_to_model = 'C:\Users\Mich\Desktop\TestSim\';
path_to_gt = 'C:\Users\Mich\Desktop\TestSim\WindowsNoEditor\AutoVrtlEnv\';
datasetName = 'SynteticDataset';
path_to_dataset = strcat('C:\Users\Mich\Desktop\',datasetName);
pc = 'velodyne\';
gt = 'label_2\';
pcToCreate = 200; % Number of pointcloud to be generated 
simulinkModel = 'simulationPointCloud.slx'; % Simulink model to be run
%% Dataset Splitting
trainingPercentage = 0.5; % Half of the samples are for training phase
listFiles = [1:pcToCreate];
listFiles = listFiles(randperm(length(listFiles))); % Random permutation
m_training = trainingPercentage*pcToCreate;
trainingFiles = listFiles(1:m_training);
validationFiles = listFiles(m_training+1:end);
trainingFiles = sort(trainingFiles);
validationFiles = sort(validationFiles);

[~ , ~ , msgID] = mkdir(path_to_dataset); % Create directory

if msgID ~= ""
    error('Already existing folder or no write permission. Delete it or check destination folder.');
end
formatTxT = '%d.ply\n';
cd(path_to_dataset);

% Create directory structure
% Training
mkdir('training');
cd('training');
mkdir('velodyne');
mkdir('label_2');
% Generation of training.txt and validation.txt which contain list of files
fileId = fopen('training.txt','w');
fprintf(fileId,formatTxT,trainingFiles);
fclose(fileId);
cd('..');
% Validation
mkdir('validation');
cd('validation');
mkdir('velodyne');
mkdir('label_2');
% Generation of training.txt and validation.txt which contain list of files
fileId = fopen('validation.txt','w');
fprintf(fileId,formatTxT,validationFiles);
fclose(fileId);
cd(path_to_model);
%% Starting generating pointclouds from syntetic LiDAR sensor
tic
for i=1:pcToCreate
    
    if ismember(i,trainingFiles)
        targetFolder = '\training\';
    else
        targetFolder='\validation\';
    end
    
    disp(['Pointcloud N.: ', int2str(i)]);
    out = sim(simulinkModel); % Start simulation of a single scene
    ptCloudData = out.ptCloudData.signals.values; % Extracting LiDAR data
    ptCloud = pointCloud(ptCloudData(:,:,:,1)); % Creating PointCloud MAtlab Object
    plyname = strcat([int2str(i)], ['.ply']); 
    fullFileNamepc = strcat(path_to_dataset,targetFolder,pc,plyname);  % Formatting position and name of the pointcloud
    pcwrite(ptCloud, fullFileNamepc, 'Encoding', 'binary'); % Write to disk
    % Now we need to move gt to the label_2 folder
    % Every time we run a simulation, it outputs a gt file which has 0 as
    % index
    nameGtFile = strcat([int2str(0)], ['.txt']);
    fullnameGtFile = strcat(path_to_gt,nameGtFile);
    fullnameDatasetGt = strcat([int2str(i)], ['.txt']); 
    movefile(fullnameGtFile, strcat(path_to_dataset,targetFolder,gt,fullnameDatasetGt));    
    
end
toc

%% Example of reading data and plot GT
idxSample = randi(pcToCreate,1);
    if ismember(idxSample,trainingFiles)
        targetFolder = '\training\';
    else
        targetFolder='\validation\';
    end
samplePc = pcread(strcat(path_to_dataset, targetFolder, pc, strcat(int2str(idxSample), '.ply') ));
gtfile = strcat(path_to_dataset, targetFolder, gt, strcat(int2str(idxSample), '.txt'));
fileID = fopen(gtfile, 'rt');
data = textscan(fileID, '%s %d %d %f %d %d %d %d %f %f %f %f %f %f %f;');
fclose(fileID);
pcshow(samplePc);
disp(strcat('Objects in the scene with ID:  ', int2str(idxSample)));
for i=1:size(data{1},1) % For each onject in the scene
    P = [data{1,12}(i) data{1,13}(i) data{1,14}(i)] ;   % you center point 
    L = [data{1,11}(i) data{1,10}(i) data{1,9}(i)] ;  % your cube dimensions 
    O = P-L/2 ;       % Get the origin of cube so that P is at center 
    plotcube(L,O,.8,[1 0 0])
    disp(data{1,1}(i))
end

